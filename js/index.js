	$(function(){
		$("[data-toggle='tooltip']").tooltip();
		$("[data-toggle='popover']").popover();
		$('.carousel').carousel({
			interval: 2000
		});
		$("#newsletterModal").on("show.bs.modal", function(e){
			console.log("El modal se está mostrando");
			$("#contactoBtn").removeClass("btn-primary");
			$("#contactoBtn").addClass("btn-outline-success");
			$("#contactoBtn").prop("disable", true);
		});
		$("#newsletterModal").on("shown.bs.modal", function(e){
			console.log("El modal fue mostrado");
		});		
		$("#newsletterModal").on("hide.bs.modal", function(e){
			console.log("El modal se está oculta");
		});
		$("#newsletterModal").on("hidden.bs.modal", function(e){
			console.log("El modal se ocultó");
			$("#contactoBtn").prop("disable", false);
		});				
	});